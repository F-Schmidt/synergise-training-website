# synergise-training-website
This gitlab repository contains all files, that are needed for the website running at 
https://www.synergise-training.eu Please note that this does not include any files of the
moodle-installation and other services.

Falko Schmidt, 2024, Germany


## ⬆ Update website

### Manual update
The update of the website can simply be done via CLI. Enter these commands:

```console
cd /tmp
git clone https://gitlab.com/F-Schmidt/synergise-training-website.git
cd synergise-training-website
chmod +x install.sh
./install.sh
# Optional: cd /tmp && rm -r synergise-training-website
```
...and you are done!

## Automatic update
Create a file using your favorite text editor (in this case we use vim):
```console
sudo vim /usr/local/bin/update-website
```
and insert the bash-commands listed in [Manual update](#manual-update). Please make sure,
to remove the downloaded files form /tmp directory, to allow multiple execution of the created program.


## 📑 Files for this website
This website will create one HTML-fie at

    /var/www/html/index.html

as well as a directory, where all resources are stored. This includes stylesheets and images and is located at

    /var/www/html/synergise-training-home-resources

No other directories are created or touched.
