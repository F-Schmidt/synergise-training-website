#!/bin/bash

echo "Removing old files..."
rm /var/www/html/index.html
rm /var/www/html/fieldtest-feedback.html
rm -r /var/www/html/synergise-training-home-resources

echo "Moving updated files to correct places..."
mv index.html /var/www/html/
mv fieldtest-feedback.html /var/www/html/
mv synergise-training-home-resources /var/www/html
